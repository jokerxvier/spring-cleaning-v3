<?php 
 $page = isset($_GET['page']) ? $_GET['page']:'';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    <!-- TITLE -->
    <title>Washla - Cleaning HTML Template</title>

    <!--  FAVICON  -->
    <link rel="shortcut icon" href="{{asset('images/icons/favicon.png')}}">

    <!-- FONT AWESOME ICONS LIBRARY -->
    <link rel="stylesheet" href="{{asset('fonts/css/all.min.css')}}">

    <!-- CSS LIBRARY STYLES -->
    <link rel="stylesheet" href="{{asset('css/lib/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/lib/bootstrap.min.css')}}">
    <link rel='stylesheet' href="{{asset('css/lib/flickity.min.css')}}">
    <link rel='stylesheet' href="{{asset('css/lib/magnific-popup.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/lib/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/lib/slick.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/lib/aos.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/navbar.css')}}">

    <!-- CSS TEMPLATE STYLES -->
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/stylesheet.css')}}">
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">

    <!-- MODERNIZR LIBRARY -->
    <script src="{{asset('js/modernizr-custom.js')}}"></script>

</head>
<body>
    @include('layouts.partial.header')

    @yield('content')

    @include('layouts.partial.footer')

<!-- JAVASCRIPTS -->
<script src="{{asset('js/lib/jquery-3.5.1.min.js')}}"></script>
<script src="{{asset('js/lib/bootstrap.min.js')}}"></script>
<script src="{{asset('js/lib/plugins.js')}}"></script>
<script src="{{asset('js/lib/nav.fixed.top.js')}}"></script>
<script src="{{asset('js/lib/contact.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/slider.js')}}"></script>


</body>
</html>