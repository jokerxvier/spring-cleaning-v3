
    <!-- FOOTER START -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-footer">
                    <h3>Loved our service?</h3>
                    <p>Share your thoughts on how we can serve you even better.</p>
                </div>
                <div class="col-lg-4 col-footer">
                    <h5>Get In Touch</h5>
                    <p>304 AC Raftel Center, Dr. A Santos Avenue,
                        <br/>Paranaque City, Philippines</p>
                    <ul class="contact">
                        <li>
                            <a href="mailto:info@washla.com">sprincleaningservices01@gmail.com</a>
                        </li>
                        <li>
                            <a href="tel:052-5401-3322"> 02) 800-5626</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4 col-footer">
                    <h5>Quick Links</h5>
                    <ul class="quick-links left-layer">
                        <li>
                            <a href="{{route('pages.home')}}">Home</a>
                        </li>
                        <li>
                            <a href="{{route('pages.about')}}">About</a>
                        </li>
                        <li>
                            <a href="s{{route('pages.franchise')}}">Franchise</a>
                        </li>
                    </ul>
                    <ul class="quick-links right-layer">
                        <li>
                            <a href="{{route('pages.gallery')}}">Gallery</a>
                        </li>
                        <li>
                            <a href="#">FAQ</a>
                        </li>
                        <li>
                            <a href="#">Terms of Service</a>
                        </li>
                    </ul>
                </div>
                
            </div>
            <hr class="footer">
            <div class="bottom-footer">
                <div class="row">
                    <div class="col-lg-6">
                        <p class="right">© 2021 Spring Cleaning Services all rights reserved.</p>
                    </div>
                    <div class="col-lg-6">
                        <ul class="footer-social">
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER END -->

    <!--SCROLL TOP START-->
    <a href="#0" class="cd-top">Top</a>
    <!--SCROLL TOP START-->
