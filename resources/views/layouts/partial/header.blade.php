
    <!-- PRELOADER START -->
    <div id="loader-wrapper">
        <div class="loader">
            <div class="ball"></div>
            <div class="ball"></div>
            <div class="ball"></div>
            <div class="ball"></div>
            <div class="ball"></div>
            <div class="ball"></div>
            <div class="ball"></div>
            <div class="ball"></div>
            <div class="ball"></div>
            <div class="ball"></div>
        </div>
    </div>
    <!-- PRELOADER END -->
    <header>
        <!-- TOP HEADER START -->
        <div class="top-header-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <ul class="top-contact text-left">
                            <li class="phone">
                                <span class="tel-no">
                                    <a href="tel:123-456-7890"> +123-456-7890</a>
                                </span>
                            </li>
                            <li class="email">
                                <a href="mailto:sprincleaningservices01@gmail.com">sprincleaningservices01@gmail.com</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-6 text-right">
                        <div class="top-social">
                            <ul class="social-list">
                                <li>
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                                <li class="m-0">
                                    <a class="btn quote-btn" href="contact.html" role="button">GET A QUOTE</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- TOP HEADER END -->

        <!-- NAV START -->
        <nav class="navbar navbar-expand-lg navbar-dark">
            <div class="container">
                <a href="/" class="navbar-brand">
                    <img src="images/spring-logo.png" alt="" style="width:280px">
                </a>

                <button type="button" class="navbar-toggler collapsed" data-toggle="collapse" data-target="#main-nav">
                    <span class="menu-icon-bar"></span>
                    <span class="menu-icon-bar"></span>
                    <span class="menu-icon-bar"></span>
                </button>

                <div id="main-nav" class="collapse navbar-collapse">
                    <ul class="navbar-nav ml-auto">
                        <li class="dropdown">
                            <a href="{{route('pages.home')}}" class="nav-item nav-link" >HOME</a>
                        </li>

                        <li>
                            <a href="{{route('service.index')}}" class="nav-item nav-link">Services</a>
                        </li>

                        <li>
                            <a href="{{route('pages.about')}}" class="nav-item nav-link">About</a>
                        </li>
                        <li>
                            <a href="{{route('pages.franchise')}}" class="nav-item nav-link">Franchise</a>
                        </li>
                        <li>
                            <a href="{{route('pages.gallery')}}" class="nav-item nav-link">Gallery</a>
                        </li>

                        <li>
                            <a href="{{route('pages.contact')}}" class="nav-item nav-link last-link-item">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- NAV END -->

        @if (\Request::route()->getName() !== 'pages.home')
            <x-banner  title="{{ $title }}" desc="A new generation of cleaning and restoration concepts."/>
        @else 
            <x-banner-slider />
        @endif
    </header>