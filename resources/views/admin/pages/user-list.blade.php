@extends('layouts.admin.admin-layout')
@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">User List</h1>

        @if(session('message-2'))
            <div class="alert alert-danger">
                {{session('message-2')}}
            </div>
       
        @elseif(session('message-4'))
            <div class="alert alert-primary">
                {{session('message-4')}}
            </div>
        @elseif(session('message-3'))
        <div class="alert alert-warning">
            {{session('message-3')}}
        </div>
        @endif
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary"></h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($data as $datas)
                    <tr>
                        <td>{{$datas->name}}</td>
                        <td>{{$datas->email}}</td>
                        <td>@if($datas->blocked==1)
                           <span class="text-warning">Blocked</span>
                            @else
                           <span class="text-primary">Active</span>
                            @endif</td>
                        <td width="160px">
                            <a href="{{route('userEdit',$datas->id)}}" class="btn btn-success btn-circle" data-toggle="tooltip"  data-placement="top" title="Edit"><i class="fas fa-edit"></i></a>
                            <span data-id="{{$datas->id}}" data-target="#DeleteModal" data-toggle="modal" ><a class="btn btn-danger btn-circle" data-toggle="tooltip"  data-placement="top" title="Delete"><i class="fas fa-trash-alt"></i></a></span>
                            @if($datas->blocked==0)
                            <span data-id="{{$datas->id}}" data-target="#BlockedModal" data-toggle="modal"><a class="btn btn-warning btn-circle" data-toggle="tooltip"  data-placement="top" title="Blocked"><i class="fas fa-ban"></i></a></span>
                            @else
                            <span data-id="{{$datas->id}}" data-target="#ActivateModal" data-toggle="modal"><a class="btn btn-primary btn-circle" data-toggle="tooltip"  data-placement="top" title="Activate"><i class="fas fa-check-circle"></i></a></span>
                            @endif
                        </td>
                    </tr>
                    @empty  
                    <p class="mb-4">There is no Available Franchiser</p>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="DeleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Deletion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure you want to Delete the User?</h6>
                    <form action="{{route('userDestroy','id')}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <div class="modal-footer">
                            <input type="hidden" name="id" id="id">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="BlockedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Blocked</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure you want to Blocked the User?</h6>
                    <form action="{{route('userBlocked','id')}}" method="POST">
                        @csrf
                        <div class="modal-footer">
                            <input type="hidden" name="id" id="id">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-warning">Blocked</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ActivateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Blocked</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure you want to Blocked the User?</h6>
                    <form action="{{route('userActivate','id')}}" method="POST">
                        @csrf
                        <div class="modal-footer">
                            <input type="hidden" name="id" id="id">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">Activate</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
   
@endsection