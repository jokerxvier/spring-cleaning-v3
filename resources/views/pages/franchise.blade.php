@extends('layouts.layout')
@section('title', 'Franchise')
@section('content')
<!-- CONTENT START -->
    <section>
        <!-- ABOUT SECTION START -->
        <div class="container mt-5 mb-5">

            <div class="row">
                <div class="col-md-8 mx-auto">
                    <x-inquiry submitButtonText="Send" url="{{  route('pages.contact.create', 'franchise')  }}"/>
                </div>
            </div>
        </div>
    </section>
@endsection