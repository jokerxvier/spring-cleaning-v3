@extends('layouts.layout')
@section('title', 'Services')
@section('content')
<!-- CONTENT START -->
<section>
    <div class="booking-services">
        <div class="container">
            <div class="intro-section">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                    <h2>Cleaning Services</h2>
                    <p>Our cleaning system sets us apart. Our customers keep coming back to us, because we provide services that work and keep their homes and apartments clean, right down to the little details. This extensive cleaning system, which we call our Detail-Clean Rotation System, which has been proven to be effective in more than five million cleans!</p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="featured">
                        <img src="{{ asset('images/featured-img.jpg') }}" alt="">
                    </div>
                    </div>
                </div>
                
            </div><!--end intro-serction-->

            <div class="main-content">
                <h1>Services</h1>
                <div class="row product">
                    <div class="col-lg-4 col-md-4 col-sm-12 item">
                        <h3>REGULAR CLEANING CONDOS</h3>
                        <div class="thumb">
                            <img src="{{ asset('images/thumb-1.png') }}" alt="">
                        </div>
                        <div class="btn-set">
                            <a href="{{ route('service.checkout') }}" class="btn book-now-btn">Book Now</a>
                            <a href="#" class="btn view-info" class="btn btn-primary" data-toggle="modal" data-target="#view-info-content">View Info</a>
                        </div>
                        <div class="modal fade" id="view-info-content" tabindex="-1" role="dialog" aria-labelledby="view-info-contente" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered rate-table" role="document">
                                    <div class="modal-content ">
                                        <div class="modal-header">
                                            <h3 class="modal-title" id="exampleModalLongTitle">REGULAR CLEANING CONDOS</h3>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table table-responsive w-100 d-block d-md-table">
                                            <thead class="thead-dark">
                                                <tr>
                                                <th scope="col">Room</th>
                                                <th scope="col">Hours</th>
                                                <th scope="col">Cleaners</th>
                                                <th scope="col">Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Studio</td>
                                                    <td>1 1/2</td>
                                                    <td>1</td>
                                                    <td>550</td>
                                                </tr>
                                                <tr>
                                                    <td>1 BR</td>
                                                    <td>2</td>
                                                    <td>1</td>
                                                    <td>700</td>
                                                </tr>
                                                <tr>
                                                    <td>2 BR</td>
                                                    <td>2</td>
                                                    <td>1</td>
                                                    <td>900</td>
                                                </tr>
                                                <tr>
                                                    <td>3 BR</td>
                                                    <td>2</td>
                                                    <td>2</td>
                                                    <td>1,100</td>
                                                </tr>
                                                <tr>
                                                    <td>4 BR</td>
                                                    <td>3</td>
                                                    <td>2</td>
                                                    <td>1,400</td>
                                                </tr>
                                                <tr>
                                                        <td>5 BR</td>
                                                    <td>3</td>
                                                    <td>2</td>
                                                    <td>1,600</td>
                                                </tr>
                                            </tbody>
                                            </table>

                                        
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary">Book Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div><!--end item-->

                    <div class="col-lg-4 col-md-4 col-sm-12 item">
                        <h3>GENERAL CLEANING CONDOS</h3>
                        <div class="thumb">
                            <img src="{{ asset('images/thumb-2.png') }}" alt="">
                        </div>
                        <div class="btn-set">
                        <a href="{{ route('service.checkout') }}" class="btn book-now-btn">Book Now</a>
                            <a href="#" class="btn view-info" class="btn btn-primary" data-toggle="modal" data-target="#view-info-content1">View Info</a>
                        </div>
                        <div class="modal fade" id="view-info-content1" tabindex="-1" role="dialog" aria-labelledby="view-info-contente" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered rate-table" role="document">
                                    <div class="modal-content ">
                                        <div class="modal-header">
                                            <h3 class="modal-title" id="exampleModalLongTitle">REGULAR CLEANING CONDOS</h3>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table table-responsive w-100 d-block d-md-table">
                                            <thead class="thead-dark">
                                                <tr>
                                                <th scope="col">Room</th>
                                                <th scope="col">Hours</th>
                                                <th scope="col">Cleaners</th>
                                                <th scope="col">Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Studio</td>
                                                    <td>2</td>
                                                    <td>2</td>
                                                    <td>950</td>
                                                </tr>
                                                <tr>
                                                    <td>1 BR</td>
                                                    <td>2</td>
                                                    <td>2</td>
                                                    <td>1300</td>
                                                </tr>

                                                <tr>
                                                    <td>2 BR</td>
                                                    <td>3</td>
                                                    <td>2</td>
                                                    <td>1500</td>
                                                </tr>

                                                <tr>
                                                    <td>3 BR</td>
                                                    <td>4</td>
                                                    <td>2</td>
                                                    <td>1800</td>
                                                </tr>
                                                <tr>
                                                    <td>4 BR</td>
                                                    <td>4</td>
                                                    <td>3</td>
                                                    <td>2,400</td>
                                                </tr>
                                                <tr>
                                                    <td>5 BR</td>
                                                    <td>5</td>
                                                    <td>3</td>
                                                    <td>2,800</td>
                                                </tr>
                                                
                                            </tbody>
                                            </table>

                                        
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary">Book Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div><!--end item-->

                    <div class="col-lg-4 col-md-4 col-sm-12 item">
                        <h3>GENERAL CLEANING RESIDENTIAL</h3>
                        <div class="thumb">
                            <img src="{{ asset('images/thumb-3.png') }}" alt="">
                        </div>
                        <div class="btn-set">
                            <a href="{{ route('service.checkout') }}" class="btn book-now-btn">Book Now</a>
                            <a href="#" class="btn view-info" class="btn btn-primary" data-toggle="modal" data-target="#view-info-content2">View Info</a>
                        </div>
                        <div class="modal fade" id="view-info-content2" tabindex="-1" role="dialog" aria-labelledby="view-info-contente" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered rate-table" role="document">
                                    <div class="modal-content ">
                                        <div class="modal-header">
                                            <h3 class="modal-title" id="exampleModalLongTitle">REGULAR CLEANING RESIDENTIAL (HOUSE&LOT)</h3>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table table-responsive w-100 d-block d-md-table">
                                            <thead class="thead-dark">
                                                <tr>
                                                <th scope="col">Room</th>
                                                <th scope="col">Hours</th>
                                                <th scope="col">Cleaners</th>
                                                <th scope="col">Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Studio</td>
                                                    <td>1 1/2</td>
                                                    <td>1</td>
                                                    <td>600</td>
                                                </tr>
                                                <tr>
                                                    <td>1 BR</td>
                                                    <td>2</td>
                                                    <td>1</td>
                                                    <td>750</td>
                                                </tr>

                                                <tr>
                                                    <td>2 BR</td>
                                                    <td>2</td>
                                                    <td>1</td>
                                                    <td>900</td>
                                                </tr>

                                                <tr>
                                                    <td>3 BR</td>
                                                    <td>2</td>
                                                    <td>2</td>
                                                    <td>1200</td>
                                                </tr>
                                                <tr>
                                                    <td>4 BR</td>
                                                    <td>3</td>
                                                    <td>2</td>
                                                    <td>1,600</td>
                                                </tr>
                                                <tr>
                                                    <td>5 BR</td>
                                                    <td>3</td>
                                                    <td>2</td>
                                                    <td>1,800</td>
                                                </tr>
                                                
                                            </tbody>
                                            </table>

                                        
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary">Book Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div><!--end item-->
                </div><!--end product-->
            </div><!--end main-content-->
        </div>
    </div>
       
</section>
@endsection