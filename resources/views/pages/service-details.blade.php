@extends('layouts.layout')
@section('title', 'Services')
@section('content')
<!-- CONTENT START -->
<section>
    <div class="booking-checkout">
        <div class="container">
            <div class="intro-text">
                <h2>Booking Your Cleaning</h2>
                <p>Its time to book our cleaning service for your home or apartment.</p>
            </div>
            <div class="checkout-section row">
                <div class="details-info col-lg-8 col-md-6 col-sm-12">
                   <h2 class="title-cleaning">Cleaning Details</h2>
                    <div class="package-content">
                       <h2>Regular Cleaning Condos</h2>
                      <table class="table table-responsive w-100 d-block d-md-table">
                          <thead>
                              <tr>
                                  <th style="width:20px;">&nbsp;</th>
                                  <th style="width:100px;">Room</th>
                                  <th>Hours</th>
                                  <th>Cleaners</td>
                                  <th>Price</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                              <td style="width:20px;"><input class="plan" name="plan" type="radio" value="studio" ></td>
                                  <td>STUDIO</td>
                                  <td>1 1/2</td>
                                  <td>1</td>
                                  <td>550</td>
                              </tr>
                              <tr>
                                 <td style="width:20px;"><input class="plan" name="plan" type="radio" value="studio" ></td>
                                  <td>1BR</td>
                                  <td>2</td>
                                  <td>1</td>
                                  <td>700</td>
                              </tr>
                              <tr>
                                   <td style="width:20px;"><input class="plan" name="plan" type="radio" value="studio" ></td>
                                  <td>2BR</td>
                                  <td>2</td>
                                  <td>1</td>
                                  <td>900</td>
                              </tr>
                              <tr>
                                   <td style="width:20px;"><input class="plan" name="plan" type="radio" value="studio" ></td>
                                  <td>3BR</td>
                                  <td>2</td>
                                  <td>2</td>
                                  <td>1,100</td>
                              </tr>
                              <tr>
                                    <td style="width:20px;"><input class="plan" name="plan" type="radio" value="studio" ></td>
                                  <td>4BR</td>
                                  <td>3</td>
                                  <td>2</td>
                                  <td>1,400</td>
                              </tr>
                              <tr>
                                    <td style="width:20px;"><input class="plan" name="plan" type="radio" value="studio" ></td>
                                  <td>4BR</td>
                                  <td>3</td>
                                  <td>2</td>
                                  <td>1,600</td>
                              </tr>
                          </tbody>
                      </table> 
                      <div class="info-text">
                          <p>NOTE: additional 300 php for every hour of extension maximum of 2 hours extension only (subject for
                           cleaners availability)
                           </p>
                      </div>
                    </div>
                    <div class="contact-details">
                        <h2>Contact Details</h2>
                        <div class="form">
                            <p>
                                <input type="text" class="text name" placeholder="name">
                            </p>
                            <p>
                                <input type="text" class="text email" placeholder="Email Address">
                            </p>
                            <p>
                                <input type="text" class="text number" placeholder="Phone Number" >
                            </p>
                            <p>
                                <input type="text" class="text address" placeholder="Address" >
                            </p>
                            <p>
                               <input type="text" class="text date" placeholder="Date" >
                           </p>
                           <p>
                               <input type="text" class="text time" placeholder="Time" >
                           </p>
                           <p>
                               <textarea name="" id="" cols="30" rows="10"></textarea>
                           </p>
                        </div>
                        <h2>ADD ONS</h2>
                        <div class="addons-btn">
                            <div class="inner">
                               <div class="row">
                                   <div class="col-md-3">
                                   <a href="#" class="btn btn-default add-ons-btn">Add Ons</a>
                                   </div>
                                   <div class="col-md-9">
                                       <div class="form-txt">
                                       SQM
                                       <input type="text" class="text addons-text">
                                       </div>
                                      
                                   </div>
                               </div>
                               <div class="selection">
                                   <p><input class="plan" name="plan" type="radio" value="studio" > GENERAL CLEANING FOR RESIDENTIAL HOUSE AND LOT, ESTABLISHMENTS AND OFFICES<span>20 php per sqm or minimum of 1,500Php (Whichever is highter)</span></p>
                                   <p><input class="plan" name="plan" type="radio" value="studio" > MOVE IN MOVE OUT FOR CONDOS, HOUSES, ESTABLISHMENTS & OFFICES<span>35 php per sqm or minimum of 3,500Php (Whichever is higher) </span></p>
                                   <p><input class="plan" name="plan" type="radio" value="studio" > DEEP CLEANING FOR CONDOMINIUM UNITS, OFFICE, 
                                   ESTABLISHMENTS AND HOUSE <span>60 php per sqm or minimum of 2,000Php (Whichever is higher) </span></p>
                               </div>
                               <div class="additional-service">
                               <h2>Additional Service
                                   <span><input class="plan" name="plan" type="radio" value="studio" > Yes</span> 
                                   <span><input class="plan" name="plan" type="radio" value="studio" > No</span> </h2>
                                   <div class="select-additional">
                                   <select class="select-css" name="car-interior-cleaning" aria-required="true" aria-invalid="false"><option value="9:00AM">Car Interior Cleaning</option><option value="10:00AM">10:00AM</option>
                                    </select>
                                    <select class="select-css" name="car-interior-cleaning " aria-required="true" aria-invalid="false"><option value="9:00AM">Car Interior Cleaning</option><option value="10:00AM">10:00AM</option>
                                    </select>
                                   </div>
                                  
                               </div>
                           
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
                <div class="sidebar col-lg-4 col-md-6 col-sm-12">
                <h2 class="title-cleaning">Booking Summary</h2>
                    <div class="details-info">
                   
                       <div class="breakdown-price">
                           <div class="cell">
                               <h3>Regular Cleaning</h3>
                               <ul>
                                   <li class="desc">Studio</li>
                                   <li class="price">550</li>
                               </ul>
                           </div>
                           <div class="cell">
                               <h3>Add Ons</h3>
                               <ul>
                                   <li class="desc">General Cleaning for Residential House and Lot</li>
                                   <li class="price">4,000</li>
                               </ul>
                           </div>
                       </div>
                       <div class="total-price">
                           <span>Total</span>
                           <span class="total">P 4,550</span>
                       </div>
                    </div>
                 
                </div>
            </div>
        </div>
    </div>
       
</section>
@endsection