@extends('layouts.layout')
@section('title','Home')
@section('content')
  <!-- HOME ABOUT START -->
  <div class="about-wrapper">
        <div class="container">
            <div class="row box-layers">
                <div class="col-lg-4">
                    <div class="ab-box white-bg box-shadow">
                        <figure class="icon ab-box-icon"> 
                            <img src="images/icons/cleaning-hand-alt.png" alt="">
                        </figure>
                        <h4>Regular Clean</h4>
                        <p>Spotless in a jiffy! A quick and no-fuss service for urbanites on the go.
                            <br/>
                            <br/>
                        </p>
                        <a href="#">Read More</a>
                    </div>
                </div>
                <div class="col-lg-4 spacing-m-center">
                    <div class="ab-box white-bg box-shadow">
                        <figure class="icon ab-box-icon">
                            <img src="images/icons/broom.png" alt="">
                        </figure>
                        <h4>General Cleaning</h4>
                        <p>Leave the heavy mess to us. An intensive, systematic cleaning service that doesn't cut corners.
                        </p>
                        <a href="#">Read More</a>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="ab-box white-bg box-shadow">
                        <figure class="icon ab-box-icon">
                            <img src="images/icons/house-cleaning.png" alt="">
                        </figure>
                        <h4>Deep clean</h4>
                        <p>Let us ELIMINATE the nasty elements from your home, that can HARM your family’s health.
                        </p>
                        <a href="#">Read More</a>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-4">
                    <div class="col-about-content">
                        <h2>WELCOME TO</h2>
                        <p>
                            <strong>Spic and span at just a click of a button!</strong>
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 spacing-m-center">
                    <div class="col-about-content">
                        <div class="dropcaps">
                            <p>
                                <span-dropcaps>A</span-dropcaps>t Spring Cleaning Services, we pride ourselves with fuss free and systematic
                                cleaning services that are handled by our group of dedicated and highly trained staff to
                                ensure utmost customer satisfaction.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="col-about-content">
                        <p>We are a professional cleaning service with vetted, experienced and hardworking staff that are committed
                            to provide topnotch cleaning services for both household and offices.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- HOME ABOUT END -->

    <!-- CONTENT START -->
    <section class=" mb-5" style="background:#f5f5f5; margin-bottom:10px !important;">
        <!-- HOME WHY US START -->
        <div class="container mt-5 mb-5">
            <div class="section-heading">
                <div class="row">
                    <div class="col-10 col-md-6 mx-auto text-center mb-4">
                        <h5 class="subtitle">WHY CHOOSE US</h5>
                        <h2>Tradition of Trust</h2>
                        <p>We specialise in intelligent & effective Search and believes in the power of partnerships to grow
                            business.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="icon-circle-layer">
                        <div class="ic-box">
                            <figure class="center-icon">
                                <img class="rotating" src="images/icons/team.png" alt="">
                            </figure>
                        </div>
                        <h4>Profesional Team</h4>
                        <p>Our team uses a sanitizing solution to wipe down light switches doorknobs.</p>
                    </div>
                </div>
                <div class="col-lg-4 spacing-m-center">
                    <div class="icon-circle-layer">
                        <div class="ic-box">
                            <figure class="center-icon">
                                <img class="rotating" src="images/icons/24-hours-phone-service.png" alt="">
                            </figure>
                        </div>
                        <h4>24/7 Services</h4>
                        <p>We encourage our customers to let us know in advance of an appointment.</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="icon-circle-layer">
                        <div class="ic-box">
                            <figure class="center-icon">
                                <img class="rotating" src="images/icons/guarantee.png" alt="">
                            </figure>
                        </div>
                        <h4>Service Guarantee</h4>
                        <p>We are telling our team members to switch out all cleaning cloths and mopheads.</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- HOME WHY US END -->



        <!-- HOME SERVICE START -->
        <div class="ws-parallax-services mb-5" style="background:url(images/banner-02.jpg) no-repeat top; background-size:cover ">
            <div class="container">
                <div class="pc-center">
                    <div class="section-heading">
                        <div class="row">
                            <div class="col-10 col-md-6 mx-auto text-center mb-4">
                                <h5 class="subtitle">WHY CHOOSE US</h5>
                                <h2>Our Great Service</h2>
                                <p>Restoring the beauty and freshness of all your upholstered fabrics and take the work out
                                    of housework for you.</p>
                            </div>
                        </div>
                    </div>
                    <div class="bs-carousel">
                        <div class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="ws-box-services">
                                    <figure class="ws-box-icon">
                                        <img src="images/icons/white-cleaning.png" alt="">
                                    </figure>
                                    <h4>Regular Cleaning Condo</h4>
                                    <p>Homes and thoroughly launder them between usage, We give our teams the accusantium doloremque
                                        laudantium.
                                    </p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="ws-box-services">
                                    <figure class="ws-box-icon">
                                        <img src="images/icons/white-service-glass-cleaning.png" alt="">
                                    </figure>
                                    <h4>Regular Cleaning Residenctial</h4>
                                    <p>We are closely monitoring national, state and local health agencies for the most recent
                                        developments.
                                    </p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="ws-box-services">
                                    <figure class="ws-box-icon">
                                        <img src="images/icons/white-cleaner.png" alt="">
                                    </figure>
                                    <h4>General Cleaning</h4>
                                    <p>Follow these tips from the CDC to help prevent the spread of the seasonal flu and respiratory
                                        diseases.
                                    </p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="ws-box-services">
                                    <figure class="ws-box-icon">
                                        <img src="images/icons/white-cleaning-tools.png" alt="">
                                    </figure>
                                    <h4>General Cleaning Residential</h4>
                                    <p>Cleanliness plays a large role in the comfort of your home, but many homeowners simply
                                        don’t have.</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="ws-box-services">
                                    <figure class="ws-box-icon">
                                        <img src="images/icons/white-vaccum-cleaner.png" alt="">
                                    </figure>
                                    <h4>Carpet Cleaning</h4>
                                    <p>We realize that every family has their own preferences, so we accommodate all your specific
                                        requests into.</p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="ws-box-services">
                                    <figure class="ws-box-icon">
                                        <img src="images/icons/white-house-cleaning.png" alt="">
                                    </figure>
                                    <h4>Apartment Cleaning</h4>
                                    <p>While some cleaning companies use rotating cleaning plans, we’re equipped to clean your
                                        entire house.</p>
                                </div>
                            </div>
                        </div>
                        <div class="owl-theme">
                            <div class="owl-controls">
                                <div class="custom-nav owl-nav"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- HOME SERVICE END -->

        <!-- TEAM START -->
        <div class="container mt-5 mb-5">
            <div class="section-heading">
                <div class="row">
                    <div class="col-10 col-md-6 mx-auto text-center mb-4">

                        <h2>Gallery</h2>
                        <p>The member of our highly experienced team is dedicated to providing you with only the best service
                            we can possibly provide.</p>
                    </div>
                </div>
            </div>
            <div class="team-carousel">
                <div class="filter-container">
                    <ul class="filter">
                        <li class="active" data-filter="*">All</li>
                        <li data-filter=".house">House</li>
                        <li data-filter=".apartment">Apartment</li>
                        <li data-filter=".office">Office</li>
                    </ul>
                </div>
                <div class="grid grid-four-col" id="kehl-grid">
                    <div class="grid-sizer"></div>
                    <div class="grid-box house">
                        <a class="image-popup-vertical-fit" href="images/gallery/scs1.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/scs1.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box house apartment">
                        <a class="image-popup-vertical-fit" href="images/gallery/scs2.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/scs2.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box apartment">
                        <a class="image-popup-vertical-fit" href="images/gallery/scs3.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/scs3.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box office">
                        <a class="image-popup-vertical-fit" href="images/gallery/scs4.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/scs4.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box house">
                        <a class="image-popup-vertical-fit" href="images/gallery/scs5.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/scs5.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box house office">
                        <a class="image-popup-vertical-fit" href="images/gallery/scs6.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/6.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box office">
                        <a class="image-popup-vertical-fit" href="images/gallery/scs7.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/scs7.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box house">
                        <a class="image-popup-vertical-fit" href="images/gallery/scs8.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/scs8.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box house office">
                        <a class="image-popup-vertical-fit" href="images/gallery/scs9.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/scs9.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box apartment">
                        <a class="image-popup-vertical-fit" href="images/gallery/scs10.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/scs10.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box office">
                        <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-1-2.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/spring-cleaning-pic-1-2.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box house">
                        <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-3.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/spring-cleaning-pic-3.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box house">
                        <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-4.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/spring-cleaning-pic-4.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box house office">
                        <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-5.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/spring-cleaning-pic-5.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box apartment">
                        <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-6.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/spring-cleaning-pic-6.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box office">
                        <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-7.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/spring-cleaning-pic-7.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box house">
                        <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-8.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/spring-cleaning-pic-7.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box office">
                        <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-9.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/spring-cleaning-pic-9.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box house">
                        <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-9.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/spring-cleaning-pic-10.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                    <div class="grid-box house">
                        <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-11.jpg">
                            <div class="image-mask"></div>
                            <img src="images/gallery/spring-cleaning-pic-11.jpg" alt="" />
                            <h3>Spring Cleaning</h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- TEAM END -->

        <!-- VIDEO POPUP START -->
        <div class="wp-pop-counter mt-5" style="background:url(images/call-us-back-bg.jpg) no-repeat; background-size:cover">
            <div class="pop-counter-content">
                <div class="btn-popup d-flex justify-content-center">
                    <a class="popup-youtube" href="https://www.youtube.com/watch?v=-_tvJtUHnmU">
                        <div class="pulse">
                            <i class="fas fa-play"></i>
                        </div>
                    </a>
                </div>
                <p>Cleaning your Worries Away</p>
                <h2>Need Help With Cleaning?</h2>
                <a href="contact.html" class="btn-washla btn-lg mt-3">Request Call Back</a>
            </div>
        </div>
        <!-- VIDEO POPUP END -->

        <!-- COUNTER START -->
        <div class="container mb-5">
            <div class="counter-bar">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="media counter-layer right-border">
                            <img src="images/icons/happy.png" class="mr-3" alt="...">
                            <div class="media-body">
                                <div class="counter" data-count="385">0</div>
                                <p>Happy Customers</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="media counter-layer right-border">
                            <img src="images/icons/counter-house-cleaning.png" class="mr-3" alt="...">
                            <div class="media-body">
                                <div class="counter" data-count="842">0</div>
                                <p>Houses Cleaned</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="media counter-layer right-border">
                            <img src="images/icons/counter-award.png" class="mr-3" alt="...">
                            <div class="media-body">
                                <div class="counter" data-count="489">0</div>
                                <p>Awards Received</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="media counter-layer">
                            <img src="images/icons/service-glass-cleaning.png" class="mr-3" alt="...">
                            <div class="media-body">
                                <div class="counter" data-count="1344">0</div>
                                <p>Glass Cleaned</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- COUNTER END -->

        <div class="container mt-5 mb-5">
            <div class="section-heading">
                <div class="row">
                    <div class="col-10 col-md-6 mx-auto text-center">
                        <h5 class="subtitle">Customer Opinions</h5>
                        <h2>Spring Cleaning Customer Says</h2>
                        <p>Spring Cleaning is a global community of practice that facilitates dialogue, information exchange and use
                            of information.</p>
                    </div>
                </div>
            </div>
            <div class="row testimonial-grid">
                <div class="col-md-6 col-lg-4">
                    <div class="testimonials-box">
                        <ul class="rating">
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                        </ul>
                        <p class="client-testimonial">
                            Really loved the service! The staff were courteous. Would definitely recommend and will book an appointment again next time!:)
                        </p>
                        <div class="media">
                            <img src="http://placehold.it/500x500" class="mr-3" alt="...">
                            <div class="media-body">
                                <h5 class="mt-0">NICKA ASPE</h5>
                                <p>Happy Customer</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="testimonials-box">
                        <ul class="rating">
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                        </ul>
                        <p class="client-testimonial">
                            I am very happy with the services provided by Chairs and Ruby yesterday. I will definitely recommend Spring Cleaning Services to my colleague and friends.
                        </p>
                        <div class="media">
                            <img src="http://placehold.it/500x500" class="mr-3" alt="...">
                            <div class="media-body">
                                <h5 class="mt-0">ALEXIS ABANO</h5>
                                <p>Happy Customer</p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6 col-lg-4">
                    <div class="testimonials-box">
                        <ul class="rating">
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="far fa-star"></i>
                            </li>
                        </ul>
                        <p class="client-testimonial">
                            Very good service!
                        </p>
                        <div class="media">
                            <img src="http://placehold.it/500x500" class="mr-3" alt="...">
                            <div class="media-body">
                                <h5 class="mt-0">DONALD SAUROMVE</h5>
                                <p>OAISIS SANTOLAN MARIKINA</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="testimonials-box">
                        <ul class="rating">
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                        </ul>
                        <p class="client-testimonial">
                            Hello! Please thank Ms. Ruby for me. I’m very satisfied with how she cleaned our house. Thank you. Until next time!
                        </p>
                        <div class="media">
                            <img src="http://placehold.it/500x500" class="mr-3" alt="...">
                            <div class="media-body">
                                <h5 class="mt-0">MICHELLE PORTO</h5>
                                <p>AMAIA STEPS BICUTAN</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="testimonials-box">
                        <ul class="rating">
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                        </ul>
                        <p class="client-testimonial">
                            Hi po! They are great and very nice, too. Thank you!
                        </p>
                        <div class="media">
                            <img src="http://placehold.it/500x500" class="mr-3" alt="...">
                            <div class="media-body">
                                <h5 class="mt-0">STEPHANIE SIA</h5>
                                <p>TRION TOWER BGC</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="testimonials-box">
                        <ul class="rating">
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                        </ul>
                        <p class="client-testimonial">
                            Thank you for a great and warm service, Ate Rowena and Gela. I’ll recommend you to my friends. God Bless!
                        </p>
                        <div class="media">
                            <img src="http://placehold.it/500x500" class="mr-3" alt="...">
                            <div class="media-body">
                                <h5 class="mt-0">GAIL UY</h5>
                                <p>ARBAS</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="testimonials-box">
                        <ul class="rating">
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                            <li>
                                <i class="fas fa-star"></i>
                            </li>
                        </ul>
                        <p class="client-testimonial">
                            I will definitely refer my friends to avail of your services. Thank you so much! Your staff are very nice and very thorough. Keep up the good work!.
                        </p>
                        <div class="media">
                            <img src="http://placehold.it/500x500" class="mr-3" alt="...">
                            <div class="media-body">
                                <h5 class="mt-0">ANONYMOUS</h5>
                                <p>Happy Customer</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- TESTIMONIALS START -->

    </section>
    <!-- CONTENT END -->
@endsection