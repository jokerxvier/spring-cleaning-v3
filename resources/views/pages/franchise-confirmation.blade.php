@component('mail::message')
# Introduction

We have successfully received your application for franchise.

@component('mail::button', ['url' => ''])
Back to Site
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
