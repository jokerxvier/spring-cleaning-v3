@extends('layouts.layout') 
@section('title', 'About')
@section('content')
 <!-- CONTENT START -->
 <section>
        <!-- ABOUT SECTION START -->
        <div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-lg-6">
                    <div class="about-info-layer">
                        <h5 class="subtitle">About Spring Cleaning</h5>
                        <h2>Cleaning made easy</h2>
                        <p>At Spring Cleaning Services, we pride ourselves with fuss free and systematic cleaning services that
                            are handled by our group of dedicated and highly trained staff to ensure utmost customer satisfaction.
                            We are a professional cleaning service with vetted, experienced and hardworking staff that are
                            committed to provide topnotch cleaning services for both household and offices.</p>

                        <!-- <div class="brand-layer d-flex">
                            <figure class="signature">
                                <img src="http://placehold.it/600x300" alt="">
                            </figure>
                            <div class="bl-contact">
                                <p>Call Us for Service</p>
                                <h4>052 5401 3322</h4>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="col-lg-6 spacing-md">
                    <figure class="about-img-layer">
                        <img src="images/about-img.jpg" alt="">
                        <div class="ai-banner">
                            <div class="media">
                                <img src="images/icons/icon-clean.png" class="mr-3" alt="...">
                                <div class="media-body">
                                    <h5 class="mt-0">Cleaning Your Worries Away</h5>
                                </div>
                            </div>
                        </div>
                    </figure>
                </div>
            </div>
        </div>


    </section>
    <section class="about-bottom">
        <div class="container">
            <div class="d-flex ">
                <div class="left-layer p-2 flex-fill">
                    <h3>OBJECTIVES</h3>
                    <ul class="ol-left order-list">
                        <li>Provide a reliable and accessible cleaning service at the tip of your fingertips.</li>
                        <li>Render unparalleled customer service. </li>
                        <li>Produce prompt and satisfactory results that will exceed our customers' expectations.</li>
                    </ul>
                </div>
                <div class="rights-layer p-2 flex-fill">
                    <h3>VISION</h3>
                    <ul class="order-list">
                        <li> Backed up by our team of dedicated and hardworking staff, we aim to be the best and most trustworthy
                            service provider for condominium, residential, and office cleaning needs in Metro Manila - Excelling
                            in reliability, honesty and exemplary works.</li>

                    </ul>
                </div>

            </div>
        </div>
    </section>
@endsection