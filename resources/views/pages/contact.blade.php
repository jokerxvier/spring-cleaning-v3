 @extends('layouts.layout')
 @section('title', 'Contact')
 @section('content')
 <!-- CONTENT START -->
 <section>
        <!-- CONTACT BOX START -->
        <div class="bg-wrapper contact-wrapper mb-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="cw-box">
                            <figure class="cw-icon">
                                <img src="images/icons/placeholder.png" alt="">
                            </figure>
                            <p>304 AC Raftel Center, Dr. A Santos Avenue</p>
                            <p>, Paranaque City, Philippines</p>
                        </div>
                    </div>
                    <div class="col-lg-4 spacing-m-center">
                        <div class="cw-box">
                            <figure class="cw-icon">
                                <img src="images/icons/telephone.png" alt="">
                            </figure>
                            <p>02) 800-5626</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="cw-box">
                            <figure class="cw-icon">
                                <img src="images/icons/email.png" alt="">
                            </figure>
                            <p>sprincleaningservices01@gmail.com</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CONTACT BOX END -->

        <!-- CONTACT FORM START -->

        <div class="container mt-5 mb-5">

            <div class="row">
                <div class="col-md-8 mx-auto">
                    <div class="contact-title">
                        <h3>Get in Touch With Us</h3>
                        <p>World’s leading non-asset- based supply chain management companies, we design and implement industry-leading.
                            We specialise in intelligent & effective search and believes business.</p>
                    </div>
                    <br/>
                    <x-inquiry submitButtonText="Send message" url="{{  route('pages.contact.create', 'inquiry')  }}"/>
                </div>
            </div>
        </div>
        <!-- CONTACT FORM END -->

        <!-- MAP START -->
        <div class="bottom-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d167616.99483399244!2d-74.08279002518668!3d40.67646407501496!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25a560db693e3%3A0xb05e8b0bdf854b54!2sGowanus%2C+Brooklyn%2C+Nueva+York%2C+EE.+UU.!5e0!3m2!1ses-419!2sdo!4v1560863423970!5m2!1ses-419!2sdo"
                class="map-iframe-alt"></iframe>
        </div>
        <!-- MAP END -->
    </section>
@endsection