@extends('layouts.layout')
@section('title', 'Gallery')
@section('content')
    <!-- CONTENT START -->
    <section>
        <!-- PROJECT GRID START -->
        <div class="container-fluid mt-5 mb-5">
            <div class="filter-container">
                <ul class="filter">
                    <li class="active" data-filter="*">All</li>
                    <li data-filter=".house">House</li>
                    <li data-filter=".apartment">Apartment</li>
                    <li data-filter=".office">Office</li>
                </ul>
            </div>
            <div class="grid grid-four-col" id="kehl-grid">
                <div class="grid-sizer"></div>
                <div class="grid-box house">
                    <a class="image-popup-vertical-fit" href="images/gallery/scs1.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/scs1.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box house apartment">
                    <a class="image-popup-vertical-fit" href="images/gallery/scs2.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/scs2.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box apartment">
                    <a class="image-popup-vertical-fit" href="images/gallery/scs3.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/scs3.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box office">
                    <a class="image-popup-vertical-fit" href="images/gallery/scs4.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/scs4.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box house">
                    <a class="image-popup-vertical-fit" href="images/gallery/scs5.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/scs5.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box house office">
                    <a class="image-popup-vertical-fit" href="images/gallery/scs6.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/6.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box office">
                    <a class="image-popup-vertical-fit" href="images/gallery/scs7.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/scs7.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box house">
                    <a class="image-popup-vertical-fit" href="images/gallery/scs8.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/scs8.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box house office">
                    <a class="image-popup-vertical-fit" href="images/gallery/scs9.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/scs9.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box apartment">
                    <a class="image-popup-vertical-fit" href="images/gallery/scs10.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/scs10.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box office">
                    <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-1-2.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/spring-cleaning-pic-1-2.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box house">
                    <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-3.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/spring-cleaning-pic-3.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box house">
                    <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-4.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/spring-cleaning-pic-4.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box house office">
                    <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-5.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/spring-cleaning-pic-5.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box apartment">
                    <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-6.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/spring-cleaning-pic-6.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box office">
                    <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-7.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/spring-cleaning-pic-7.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box house">
                    <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-8.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/spring-cleaning-pic-7.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box office">
                    <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-9.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/spring-cleaning-pic-9.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box house">
                    <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-9.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/spring-cleaning-pic-10.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box house">
                    <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-11.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/spring-cleaning-pic-11.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box house">
                    <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-12.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/spring-cleaning-pic-12.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box house">
                    <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-13.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/spring-cleaning-pic-13.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
                <div class="grid-box house">
                    <a class="image-popup-vertical-fit" href="images/gallery/spring-cleaning-pic-15.jpg">
                        <div class="image-mask"></div>
                        <img src="images/gallery/spring-cleaning-pic-15.jpg" alt="" />
                        <h3>Spring Cleaning</h3>
                    </a>
                </div>
            </div>
        </div>
        <!-- PROJECT GRID END -->
    </section>
    <!-- CONTENT END -->
@endsection