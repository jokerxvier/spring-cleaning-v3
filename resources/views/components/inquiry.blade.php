<form  method="POST" action="{{ $url}}">
    @csrf
    <div class="messages">
        <x-alert />
    </div>
    <div class="controls">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <input id="form_name" type="text" name="name" class="form-control custom-form @error('name') is-invalid @enderror" placeholder="*Name" required="required" data-error="Firstname is required.">
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <input id="form_email" type="email" name="email" class="form-control custom-form @error('email') is-invalid @enderror" placeholder="*Email address" required="required">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <input id="form_phone" type="tel" name="phone" class="form-control custom-form @error('phone') is-invalid @enderror" placeholder="*Please enter your phone">
                    @error('phone')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <textarea id="form_message" name="message" class="form-control message-form custom-form @error('message') is-invalid @enderror" placeholder="*Your message" rows="6"></textarea>
                    @error('message')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <p>
                    <button type="submit" class="btn btn-washla">{{ $submitButtonText }}</button>
                </p>
            </div>

        </div>
    </div>
</form>