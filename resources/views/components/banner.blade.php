<div class="pages-hero" style="background:url({{asset('images/banner-bg-about.jpg')}}) no-repeat center;  background-size:cover;"">
    <div class="container">
        <div class="pages-title">
            <h1>{{ $title  }}</h1>
            <p>{{ $desc }}</p>
        </div>
    </div>
</div>