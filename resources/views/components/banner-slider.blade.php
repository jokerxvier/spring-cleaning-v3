<!--SLIDER START-->
<div class="home-slider">
    <!-- partial:index.partial.html -->
    <div class="hero-slider" data-carousel>
        <div class="carousel-cell" style="background:url(images/spring-banner-01.jpg) no-repeat bottom center; background-size:cover;">
            <div class="overlay"></div>
            <div class="container slider-caption">
                <h5 class="subtitle"> </h5>
                <h2 class="title">Don't stress
                    <br/> about your
                    <br/> mess, we clean as
                    <br/> you please</h2>
                <div class="slider-btn">
                    <a href="{{route('pages.contact')}}" class="btn-washla btn-outline-blue btn-lg">Contact Us</a>
                    <!-- <div class="btn-popup">
                    <a class="popup-youtube" href="https://www.youtube.com/watch?v=-_tvJtUHnmU">
                        <div class="pulse"><i class="fas fa-play"></i></div>
                    </a>
                </div> -->
                </div>
            </div>
        </div>
        <div class="carousel-cell" style="background:url(images/banner-021.jpg) no-repeat bottom center; background-size:cover;">
            <div class="overlay"></div>
            <div class="container slider-caption">
                <h5 class="subtitle">We clean. A lot.</h5>
                <h2 class="title">Because quality
                    <br/> is necessary</h2>
                <div class="slider-btn">
                    <a href="{{route('pages.contact')}}" class="btn-washla btn-outline-blue btn-lg">Contact Us</a>
                    <!-- <div class="btn-popup">
                    <a class="popup-youtube" href="https://www.youtube.com/watch?v=-_tvJtUHnmU">
                        <div class="pulse"><i class="fas fa-play"></i></div>
                    </a>
                </div> -->
                </div>
            </div>

        </div>
        <div class="carousel-cell" style="background:url(images/banner-3.jpg) no-repeat bottom center; background-size:cover;">
            <div class="overlay"></div>
            <div class="container slider-caption">
                <h5 class="subtitle">Renew your look</h5>
                <h2 class="title">A tradition of
                    <br/>quality cleaning</h2>
                <div class="slider-btn">
                    <a href="{{route('pages.contact')}}" class="btn-washla btn-outline-white btn-lg">Contact Us</a>
                    <!-- <div class="btn-popup">
                    <a class="popup-youtube" href="https://www.youtube.com/watch?v=-_tvJtUHnmU">
                        <div class="pulse"><i class="fas fa-play"></i></div>
                    </a>
                </div> -->
                </div>
            </div>
        </div>
    </div>
    <!-- partial -->
</div>
<!--SLIDER END-->