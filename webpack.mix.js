const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps();

mix.sass('resources/admin/css/sb-admin-2.min.scss', 'public/admin/css')
    .sass('resources/admin/css/sb-admin-2.scss','public/admin/css')
    .sass('resources/admin/css/sb-admin-style.scss','public/admin/css');

mix.copyDirectory('resources/admin/img', 'public/admin/img')
    .copyDirectory('resources/admin/js','public/admin/js')
    .copyDirectory('resources/admin/scss', 'public/admin/scss')
    .copyDirectory('resources/admin/vendor','public/admin/vendor');
