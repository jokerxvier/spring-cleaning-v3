<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{

    public function home(){
        $title = 'Home';
        return view('pages.index', compact('title'));
    }
    public function about()
    {
        $title ='About';
        return view('pages.about',compact('title'));
    }
    public function contact()
    {   $title = 'Contact';
        return view('pages.contact',compact('title'));
    }
    public function franchise()
    {
        $title = 'Franchise';
        return view('pages.franchise',compact('title'));
    }
    public function gallery(){
        $title = 'Gallery';
        return view('pages.gallery', compact('title'));
    }
    public function admin(){
        return view('admin.pages.admin');
    }
}
