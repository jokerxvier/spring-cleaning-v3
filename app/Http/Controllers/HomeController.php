<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Franchiser;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.pages.admin');
    }
    public function tables()
    {
        $franchiserData = Franchiser::get();
        return view('admin.pages.franchise',compact('franchiserData'));
    }

    public function userCreate()
    {
        return view ('admin.pages.user-management');
    }
   
    public function userStore(Request $request)
    {

        $validatedUser =request()->validate([
            'name'=>'required',
            'email'=>'required|email|unique:users',
            'password'=>'required|string|min:8|confirmed'
        ]);
        $validatedUser['password'] = Hash::make($validatedUser['password']);
        User::create($validatedUser);
        return redirect(route('userCreate'))->with('message','Succesfully added a User');
    }
    public function userList()
    {
        $data = User::get();
        return view('admin.pages.user-list',compact('data'));
    }
    public function userEdit($id)
    {
        $user = User::findorfail($id);
        return view('admin.pages.edit-user',compact('user'));
    }

    public function userUpdate(Request $request)
    {
        $validatedUser = request()->validate([
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required|string|min:8|confirmed'
        ]);

        User::findorfail($request->id)->update($validatedUser);
        return redirect(route('userEdit',$request->id))->with('message','Successfully Updated');
    }

    public function userDestroy(Request $request)
    {
        $user = User::findorfail($request->id);
        
        $user->delete();
        return redirect(route('userList'))->with('message-2','Deleted Successfully!');

    }
    public function userBlocked(Request $request)
    {
        $user = User::findorfail($request->id);
        $user->blocked=1;
        $user->save();
        return redirect(route('userList'))->with('message-3','Successfully Banned');
    }
    public function userActivate(Request $request)
    {
        $user = User::findorfail($request->id);
        $user->blocked=0;
        $user->save();
        return redirect(route('userList'))->with('message-4','Activated Succesfully');
    }

   
}
