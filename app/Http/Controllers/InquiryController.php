<?php

namespace App\Http\Controllers;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\CreateInquiryRequest;
use App\Models\Inquiry;

class InquiryController extends Controller
{
    public function index()
    {
        
    }

    public function createInquiry($type, CreateInquiryRequest $request)
    {
        $validated = collect($request->validated());
        $type = $type && collect(Inquiry::INQUIRY_TYPE)->contains($type) ? $type  : false;

        if (!$type) {
            return redirect()->back()->with('alert','Opps! Something went wrong!');
        } 

        $validated = $validated->merge(['type' => $type]);

        Inquiry::upsert($validated->toArray(), ['email', 'type']);

        return redirect()->back()->with('success','Your inquiry has been sent');
    }

    public function inquiryList($type) 
    {
        $title = $type === 'franchise' ? 'Franchise List' : 'Contact List';

        $type = $type && collect(Inquiry::INQUIRY_TYPE)->contains($type) ? $type  :false;

        if (!$type) {
            abort(404);
        }

        $inquiries = Inquiry::whereType($type)->paginate(10);

        return view('admin.pages.franchise', compact('inquiries', 'title'));
    }

   
}
