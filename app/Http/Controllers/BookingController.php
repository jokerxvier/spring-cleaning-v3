<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookingController extends Controller
{
    public function index()
    {
        $title = "Cleaning Services";
        return view('pages.service', compact('title'));
    }

    public function checkout()
    {
        $title = "Cleaning Services";
        return view('pages.service-details', compact('title'));
    }
}
