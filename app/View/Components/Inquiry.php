<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Inquiry extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $url;
    public $submitButtonText;



    public function __construct($url, $submitButtonText)
    {
        $this->url = $url;
        $this->submitButtonText = $submitButtonText;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.inquiry');
    }
}
