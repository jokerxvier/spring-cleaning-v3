<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\FranchiserController;
use App\Http\Controllers\InquiryController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\Admin\DashboardController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[PagesController::class, 'home'])->name('pages.home');

Route::prefix('contact')->group(function () {
    Route::get('/',[PagesController::class, 'contact'])->name('pages.contact');
    Route::post('/{type}',[InquiryController::class, 'createInquiry'])->name('pages.contact.create');
});

Route::prefix('services')->group(function () {
    Route::get('/',[BookingController::class, 'index'])->name('service.index');
    Route::get('/checkout',[BookingController::class, 'checkout'])->name('service.checkout');
});

Route::get('/about',[PagesController::class, 'about'])->name('pages.about');
Route::get('/gallery',[PagesController::class, 'gallery'])->name('pages.gallery');
Route::get('/franchise',[PagesController::class, 'franchise'])->name('pages.franchise');
Route::post('/franchise/send', [FranchiserController::class, 'store'])->name('franchiser.store');

Auth::routes();



Route::prefix('admin')->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('admin');
    Route::get('/user-create', [App\Http\Controllers\HomeController::class, 'userCreate'])->name('userCreate');
    Route::POST('/user-create/store', [App\Http\Controllers\HomeController::class, 'userStore'])->name('userStore');
    Route::get('/user-list', [App\Http\Controllers\HomeController::class, 'userList'])->name('userList');
    Route::get('/user-edit/{id}', [App\Http\Controllers\HomeController::class, 'userEdit'])->name('userEdit');
    Route::POST('/user-update/{id}', [App\Http\Controllers\HomeController::class, 'userUpdate'])->name('userUpdate');
    Route::DELETE('/user-delete/{id}', [App\Http\Controllers\HomeController::class, 'userDestroy'])->name('userDestroy');
    Route::POST('/user-blocked/{id}', [App\Http\Controllers\HomeController::class, 'userBlocked'])->name('userBlocked');
    Route::POST('/user-activate/{id}', [App\Http\Controllers\HomeController::class, 'userActivate'])->name('userActivate');

    Route::get('/inquiry/{type}', [InquiryController::class, 'inquiryList'])->name('inquiry.type');
});

